FROM golang:1.14.6-alpine3.12 as builder
COPY go.mod go.sum /go/src/gitlab.com/inazarinBot/
WORKDIR /go/src/gitlab.com/inazarinBot
RUN go mod download
COPY . /go/src/gitlab.com/inazarinBot
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
# RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/inazarinBot gitlab.com/inazarinBot
# RUN go run main.go

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
# COPY --from=builder /go/src/gitlab.com/inazarinBot/build/inazarinBot /usr/bin/inazarinBot
COPY --from=builder /go/src/gitlab.com/inazarinBot/main .
COPY --from=builder /go/src/gitlab.com/inazarinBot/.env .
COPY --from=builder /go/src/gitlab.com/inazarinBot/api_token .
EXPOSE 8084 8084
CMD ["./main"]

# ENTRYPOINT ["/usr/bin/inazarinBot"]
