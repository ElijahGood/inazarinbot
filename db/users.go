package db

import (
	"database/sql"
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/ElijahGood/inazarinbot/models"
)

func (db Database) GetAllUsers() (*models.UsersList, error) {
	list := &models.UsersList{}
	rows, err := db.Conn.Query("SELECT * FROM users ORDER BY id DESC")
	if err != nil {
		return list, err
	}
	for rows.Next() {
		var item models.User
		err := rows.Scan(&item.ID, &item.Username, &item.First_name, &item.Created_at)
		if err != nil {
			return list, err
		}
		list.Users = append(list.Users, item)
	}
	return list, nil
}

func (db Database) GetUserById(userId int) (models.User, error) {
	item := models.User{}
	query := `SELECT * FROM users WHERE chat_id = $1;`
	row := db.Conn.QueryRow(query, userId)
	switch err := row.Scan(&item.ID, &item.ChatId, &item.Username, &item.First_name, &item.Last_name, &item.Language_code, &item.Is_bot, &item.Created_at); err {
	case sql.ErrNoRows:
		return item, ErrNoMatch
	default:
		return item, err
	}
}

func (db Database) CreateUser(item *tgbotapi.User, chat_id int64) error {
	var id int
	query := `INSERT INTO users 
	(chat_id, username, first_name, last_name, language_code, is_bot)
	VALUES ($1, $2, $3, $4, $5, $6) 
	RETURNING id;`
	fmt.Printf("		ISERTING: %s, %s, %s, %s, %t \n", item.UserName, item.FirstName, item.LastName, item.LanguageCode, item.IsBot)
	err := db.Conn.QueryRow(query, chat_id, item.UserName, item.FirstName, item.LastName, item.LanguageCode, false).Scan(&id)
	if err != nil {
		return err
	}
	item.ID = id
	return nil
}
