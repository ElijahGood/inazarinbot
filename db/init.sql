CREATE TABLE IF NOT EXISTS messages(
    id SERIAL PRIMARY KEY,
    username_id	int,
    chat_id int,
    msg text,
    answer text,
    msg_date int
);

CREATE TABLE IF NOT EXISTS users(
    id SERIAL PRIMARY KEY,
    chat_id int,
    username varchar(30),
    first_name varchar(50),
    last_name varchar(50),
    language_code varchar(2),
    is_bot boolean NOT NULL DEFAULT false,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);