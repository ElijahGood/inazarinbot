This project represents simple TelegramBot sceleton with message handling (via Telegram Bot API - https://github.com/go-telegram-bot-api/telegram-bot-api), dealing with users (saving them to postgres db).
To start your project get yours telegram-bot TOKEN and paste it to 'api_token' textfile. You can find empty file in root of this project.
Then edit .env file with your preferable credentials.
Beware that this project performs simple logic - just answering every input message with the exat same message as an answer.
