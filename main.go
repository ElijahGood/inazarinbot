package main

import (
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/ElijahGood/inazarinbot/db"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var InazarinApiKey string

func init() {
	key, err := ioutil.ReadFile("./api_token")
	if err != nil {
		panic(err)
	} else {
		InazarinApiKey = string(key)
	}
}

func main() {
	dbUser, dbPassword, dbName :=
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB")
	database, err := db.Initialize(dbUser, dbPassword, dbName)
	if err != nil {
		log.Fatalf("Could not set up database: %v", err)
	}
	defer database.Conn.Close()

	bot, err := tgbotapi.NewBotAPI(InazarinApiKey)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		_, err := database.GetUserById(update.Message.From.ID)
		if err != nil {
			err := database.CreateUser(update.Message.From, update.Message.Chat.ID)
			if err != nil {
				log.Printf("[%s]	 --->>>		WAS UNABLE TO SAVE NEW USER:	%s\n", update.Message.From.UserName, err)
			}
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		msg.ReplyToMessageID = update.Message.MessageID

		bot.Send(msg)
	}
}
