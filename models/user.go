package models

import "time"

type User struct {
	ID            int       `json:"id"`
	ChatId        int       `json:"chat_id"`
	Username      string    `json:"username"`
	First_name    string    `json:"first_name"`
	Last_name     string    `json:"last_name"`
	Language_code string    `json:"language_code"`
	Is_bot        bool      `json:"is_bot"`
	Created_at    time.Time `json:"сreated_at"`
}

type UsersList struct {
	Users []User `json:"users"`
}
